import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ComposantsComponent } from './composants/composants.component';
import { FooterModule } from './composants/footer/footer.module';
import { NavbarComponent } from './composants/navbar/navbar.component';
import { SidebarModule } from './composants/sidebar/sidebar.module';
import { LayoutsComponent } from './layouts/layouts.component';
import { VueLayerComponent } from './layouts/vue-layer/vue-layer.component';
import { AuthLayerComponent } from './layouts/auth-layer/auth-layer.component';
import { PagesComponent } from './pages/pages.component';
import { MapModule } from './pages/map/maps.module';
import { EvolutionAnalyserModule } from './pages/evolution-analyser/evolution-analyser.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DeclarationsModules } from './pages/declarations/declarations.module';
import { DepartementModule } from './pages/departement/departement.module';
import { InterventionModule } from './pages/intervention/intervention.module';
import { RegionsModule } from './pages/regions/regions.module';
import { UsersModule } from './pages/users/users.module';
import { NgxBootstrapTreeviewModule } from 'ngx-bootstrap-treeview';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ModalModule } from 'ngx-bootstrap/modal';
import { Covid19IndexComponent } from './pages/covid19-index/covid19-index.component';
import { MaterialModule } from './pages/material/material.module';
// import { AgmCoreModule } from '@agm/core';

@NgModule({
  declarations: [
    AppComponent,
    ComposantsComponent,
    NavbarComponent,
    LayoutsComponent,
    VueLayerComponent,
    AuthLayerComponent,
    PagesComponent,
    Covid19IndexComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MapModule,
    EvolutionAnalyserModule,
    NgbModule,
    SidebarModule,
    FooterModule,
    DeclarationsModules,
    DepartementModule,
    InterventionModule,
    RegionsModule,
    UsersModule,
    NgxBootstrapTreeviewModule,
    BrowserAnimationsModule,
    ModalModule.forRoot(),
    MaterialModule,
    // AgmCoreModule.forRoot({
    //   apiKey: 'AIzaSyCtf5GuVDzKh5dcxeRyOzmQ39Cn97R9czI'
    // })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
