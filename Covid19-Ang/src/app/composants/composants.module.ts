import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import {FooterModule} from "./footer/footer.module";
import {NavbarComponent} from "./navbar/navbar.component";
import { SidebarModule } from './sidebar/sidebar.module';

@NgModule({
  imports: [CommonModule, RouterModule, NgbModule,SidebarModule,FooterModule],
  declarations: [ NavbarComponent ],
  exports: [ NavbarComponent ]
})

export class ComposantsModule {
  constructor() {}
}
