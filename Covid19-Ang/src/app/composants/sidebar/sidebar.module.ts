import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import {SidebarComponent} from '../sidebar/sidebar.component'
@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [SidebarComponent],
  exports: [SidebarComponent]
})

export class SidebarModule {
  constructor() {}
}
