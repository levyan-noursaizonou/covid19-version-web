import { Component, OnInit } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}

export const ROUTES: RouteInfo[] = [
  {
    path: "/declarations",
    title: "declarations",
    icon: "icon-paper",
    class: ""
  },
  {
    path: "/regions",
    title: "regions",
    icon: "icon-square-pin",
    class: ""
  },
  {
    path: "/departments",
    title: "departments",
    icon: "icon-puzzle-10",
    class: ""
  },
  {
    path: "/maps",
    title: "maps",
    icon: "icon-pin",
    class: ""
  },
  {
    path: "/users",
    title: "Teams",
    icon: "icon-single-02",
    class: ""
  },
  {
    path: "/evolution-analyser",
    title: "evolution-analyser",
    icon: "icon-user-run",
    class: ""
  },
  {
    path: "/interventions",
    title: "interventions",
    icon: "icon-molecule-40",
    class: ""
  },
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor() {}

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    console.log(this.menuItems);
  }
  isMobileMenu() {
    if (window.innerWidth > 991) {
      return false;
    }
    return true;
  }
}
