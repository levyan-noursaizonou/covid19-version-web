import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from "@angular/common";
import { BrowserModule } from "@angular/platform-browser";

import {VueLayerComponent} from "./layouts/vue-layer/vue-layer.component";
// import {} from "../app/layouts/vue-layer/vue-layer.module"
import {AuthLayerComponent} from "./layouts/auth-layer/auth-layer.component"
const routes: Routes = [
  {
    path: "",
    redirectTo: "Covid19",
    pathMatch: "full"
  },
  {
    path: "",
    component: VueLayerComponent,
    children: [
      {
        path: "",
        loadChildren: () => import('./layouts/vue-layer/vue-layer.module').then(m => m.VueLayerModule)
          // "./layouts/vue-layer/vue-layer.module#VueLayerModule"
      }
    ]
  },
  {
    path: "",
    component: AuthLayerComponent,
    // children: [
    //   {
    //     path: "",
    //     loadChildren:
    //       "./layouts/auth-layer/auth-layer.module#AuthLayerModule"
    //   }
    // ]
  },
  {
    path: "**",
    redirectTo: "Covid19 /not found"
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes,{useHash: true}),
    CommonModule,
    BrowserModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
