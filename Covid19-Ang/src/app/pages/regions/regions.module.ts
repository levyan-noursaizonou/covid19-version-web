import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';

import {RegionsComponent} from '../regions/regions.component'

@NgModule({
  imports: [CommonModule],
  declarations: [RegionsComponent],
  exports: [RegionsComponent]
})
export class RegionsModule{
  constructor(){}
}
