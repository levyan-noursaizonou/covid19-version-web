import { Component, OnInit,TemplateRef } from '@angular/core';
// import AllRegion from '../../../assets/img/img_reg/AllRegion.json'
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
declare interface depart{
  case: number;
  name: string;
}
declare interface formRegion{
  label: string,
  pathload: string;
  firstSvg: string;
  case: number;
  departement_list: depart[]
}

export const regionlibrary: formRegion[]= [
  {
    label:'Dakar',
    pathload: '../../../assets/img/img_reg/dakar.svg',
    firstSvg: '../../../assets/img/vide2.png',
    case: 1000,
    departement_list: [
      {
        "case": 2,
        "name": "Diamniadio"
      },
      {
        "case": 2,
        "name": "Keurmassar"
      },
      {
        "case": 2,
        "name": "Plateau"
      },
      {
        "case": 1,
        "name": "Gueuletape"
      },
      {
        "case": 1,
        "name": "Medina"
      },
      {
        "case": 1,
        "name": "Sicapkarack"
      },
      {
        "case": 2,
        "name": "Guediawaye"
      },
      {
        "case": 6,
        "name": "Pikine"
      }
    ]
  },
  {
    label: 'Diourbel',
    pathload: '../../../assets/img/img_reg/diourbel.svg',
    firstSvg: '../../../assets/img/vide3.jpg',
    case: 1000,
    departement_list: [
      {
        "case": 2,
        "name": "Diamniadio"
      },
      {
        "case": 2,
        "name": "Keurmassar"
      },
      {
        "case": 2,
        "name": "Plateau"
      },
      {
        "case": 1,
        "name": "Gueuletape"
      },
      {
        "case": 1,
        "name": "Medina"
      },
      {
        "case": 1,
        "name": "Sicapkarack"
      },
      {
        "case": 2,
        "name": "Guediawaye"
      },
      {
        "case": 6,
        "name": "Pikine"
      }
    ]
  },
  {
    label: 'Fatick',
    pathload: '../../../assets/img/img_reg/fatick.svg',
    firstSvg: '../../../assets/img/vide4.svg',
    case: 1000,
    departement_list: [
      {
        "case": 2,
        "name": "Diamniadio"
      },
      {
        "case": 2,
        "name": "Keurmassar"
      },
      {
        "case": 2,
        "name": "Plateau"
      },
      {
        "case": 1,
        "name": "Gueuletape"
      },
      {
        "case": 1,
        "name": "Medina"
      },
      {
        "case": 1,
        "name": "Sicapkarack"
      },
      {
        "case": 2,
        "name": "Guediawaye"
      },
      {
        "case": 6,
        "name": "Pikine"
      }
    ]
  },
  {
    label: 'Kaffrine',
    pathload: '../../../assets/img/img_reg/kaffrine.svg',
    firstSvg: '../../../assets/img/vide5.jpg',
    case: 1000,
    departement_list: [
      {
        "case": 2,
        "name": "Diamniadio"
      },
      {
        "case": 2,
        "name": "Keurmassar"
      },
      {
        "case": 2,
        "name": "Plateau"
      },
      {
        "case": 1,
        "name": "Gueuletape"
      },
      {
        "case": 1,
        "name": "Medina"
      },
      {
        "case": 1,
        "name": "Sicapkarack"
      },
      {
        "case": 2,
        "name": "Guediawaye"
      },
      {
        "case": 6,
        "name": "Pikine"
      }
    ]
  },
  {
    label: 'Kaolack',
    pathload: '../../../assets/img/img_reg/kaolack.svg',
    firstSvg: '../../../assets/img/vide6.jpg',
    case: 1000,
    departement_list: [
      {
        "case": 2,
        "name": "Diamniadio"
      },
      {
        "case": 2,
        "name": "Keurmassar"
      },
      {
        "case": 2,
        "name": "Plateau"
      },
      {
        "case": 1,
        "name": "Gueuletape"
      },
      {
        "case": 1,
        "name": "Medina"
      },
      {
        "case": 1,
        "name": "Sicapkarack"
      },
      {
        "case": 2,
        "name": "Guediawaye"
      },
      {
        "case": 6,
        "name": "Pikine"
      }
    ]
  },
  {
    label: 'Kedougou',
    pathload: '../../../assets/img/img_reg/kedougou.svg',
    firstSvg: '../../../assets/img/vide2.png',
    case: 1000,
    departement_list: [
      {
        "case": 2,
        "name": "Diamniadio"
      },
      {
        "case": 2,
        "name": "Keurmassar"
      },
      {
        "case": 2,
        "name": "Plateau"
      },
      {
        "case": 1,
        "name": "Gueuletape"
      },
      {
        "case": 1,
        "name": "Medina"
      },
      {
        "case": 1,
        "name": "Sicapkarack"
      },
      {
        "case": 2,
        "name": "Guediawaye"
      },
      {
        "case": 6,
        "name": "Pikine"
      }
    ]
  },
  {
    label: 'Kolda',
    pathload: '../../../assets/img/img_reg/kolda.svg',
    firstSvg: '../../../assets/img/vide2.png',
    case: 1000,
    departement_list: [
      {
        "case": 2,
        "name": "Diamniadio"
      },
      {
        "case": 2,
        "name": "Keurmassar"
      },
      {
        "case": 2,
        "name": "Plateau"
      },
      {
        "case": 1,
        "name": "Gueuletape"
      },
      {
        "case": 1,
        "name": "Medina"
      },
      {
        "case": 1,
        "name": "Sicapkarack"
      },
      {
        "case": 2,
        "name": "Guediawaye"
      },
      {
        "case": 6,
        "name": "Pikine"
      }
    ]
  },
  {
    label: 'Louga',
    pathload: '../../../assets/img/img_reg/louga.svg',
    firstSvg: '../../../assets/img/vide2.png',
    case: 1000,
    departement_list: [
      {
        "case": 2,
        "name": "Diamniadio"
      },
      {
        "case": 2,
        "name": "Keurmassar"
      },
      {
        "case": 2,
        "name": "Plateau"
      },
      {
        "case": 1,
        "name": "Gueuletape"
      },
      {
        "case": 1,
        "name": "Medina"
      },
      {
        "case": 1,
        "name": "Sicapkarack"
      },
      {
        "case": 2,
        "name": "Guediawaye"
      },
      {
        "case": 6,
        "name": "Pikine"
      }
    ]
  },
  {
    label: 'Matam',
    pathload: '../../../assets/img/img_reg/matam.svg',
    firstSvg: '../../../assets/img/vide2.png',
    case: 1000,
    departement_list: [
      {
        "case": 2,
        "name": "Diamniadio"
      },
      {
        "case": 2,
        "name": "Keurmassar"
      },
      {
        "case": 2,
        "name": "Plateau"
      },
      {
        "case": 1,
        "name": "Gueuletape"
      },
      {
        "case": 1,
        "name": "Medina"
      },
      {
        "case": 1,
        "name": "Sicapkarack"
      },
      {
        "case": 2,
        "name": "Guediawaye"
      },
      {
        "case": 6,
        "name": "Pikine"
      }
    ]
  },
  {
    label: 'Saint-Louis',
    pathload: '../../../assets/img/img_reg/saintlouis.svg',
    firstSvg: '../../../assets/img/vide2.png',
    case: 1000,
    departement_list: [
      {
        "case": 2,
        "name": "Diamniadio"
      },
      {
        "case": 2,
        "name": "Keurmassar"
      },
      {
        "case": 2,
        "name": "Plateau"
      },
      {
        "case": 1,
        "name": "Gueuletape"
      },
      {
        "case": 1,
        "name": "Medina"
      },
      {
        "case": 1,
        "name": "Sicapkarack"
      },
      {
        "case": 2,
        "name": "Guediawaye"
      },
      {
        "case": 6,
        "name": "Pikine"
      }
    ]
  },
  {
    label: 'Ziguinchor',
    pathload: '../../../assets/img/img_reg/ziguinchor.svg',
    firstSvg: '../../../assets/img/vide2.png',
    case: 1000,
    departement_list: [
      {
        "case": 2,
        "name": "Diamniadio"
      },
      {
        "case": 2,
        "name": "Keurmassar"
      },
      {
        "case": 2,
        "name": "Plateau"
      },
      {
        "case": 1,
        "name": "Gueuletape"
      },
      {
        "case": 1,
        "name": "Medina"
      },
      {
        "case": 1,
        "name": "Sicapkarack"
      },
      {
        "case": 2,
        "name": "Guediawaye"
      },
      {
        "case": 6,
        "name": "Pikine"
      }
    ]
  },
  {
    label: 'Tambacounda',
    pathload: '../../../assets/img/img_reg/tambacounda.svg',
    firstSvg: '../../../assets/img/vide2.png',
    case: 1000,
    departement_list: [
      {
        "case": 2,
        "name": "Diamniadio"
      },
      {
        "case": 2,
        "name": "Keurmassar"
      },
      {
        "case": 2,
        "name": "Plateau"
      },
      {
        "case": 1,
        "name": "Gueuletape"
      },
      {
        "case": 1,
        "name": "Medina"
      },
      {
        "case": 1,
        "name": "Sicapkarack"
      },
      {
        "case": 2,
        "name": "Guediawaye"
      },
      {
        "case": 6,
        "name": "Pikine"
      }
    ]
  }
  ,
  {
    label: 'Thies',
    pathload: '../../../assets/img/img_reg/thies.svg',
    firstSvg: '../../../assets/img/vide2.png',
    case: 1000,
    departement_list: [
      {
        "case": 2,
        "name": "Diamniadio"
      },
      {
        "case": 2,
        "name": "Keurmassar"
      },
      {
        "case": 2,
        "name": "Plateau"
      },
      {
        "case": 1,
        "name": "Gueuletape"
      },
      {
        "case": 1,
        "name": "Medina"
      },
      {
        "case": 1,
        "name": "Sicapkarack"
      },
      {
        "case": 2,
        "name": "Guediawaye"
      },
      {
        "case": 6,
        "name": "Pikine"
      }
    ]
  },
  {
    label: 'Sedhiou',
    pathload: '../../../assets/img/img_reg/sedhiou.svg',
    firstSvg: '../../../assets/img/vide2.png',
    case: 1000,
    departement_list: [
      {
        "case": 2,
        "name": "Diamniadio"
      },
      {
        "case": 2,
        "name": "Keurmassar"
      },
      {
        "case": 2,
        "name": "Plateau"
      },
      {
        "case": 1,
        "name": "Gueuletape"
      },
      {
        "case": 1,
        "name": "Medina"
      },
      {
        "case": 1,
        "name": "Sicapkarack"
      },
      {
        "case": 2,
        "name": "Guediawaye"
      },
      {
        "case": 6,
        "name": "Pikine"
      }
    ]
  }

]
@Component({
  selector: 'app-regions',
  templateUrl: './regions.component.html',
  styleUrls: ['./regions.component.scss']
})

export class RegionsComponent implements OnInit {
  regions: any[];
  modalRef: BsModalRef;
  constructor(private modalService: BsModalService) { }

  ngOnInit(): void {
    this.regions=regionlibrary.filter(RegionItem => RegionItem)
    console.log(this.regions);
  }

  openModalWithClass(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(
      template, Object.assign({}, { class: 'gray modal-lg' })
    );}
}
