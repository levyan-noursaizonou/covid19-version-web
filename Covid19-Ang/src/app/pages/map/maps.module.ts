import { NgModule } from "@angular/core";
// import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';

import {MapComponent} from "./maps.component";
// import { LegendService, MarkerService, MapsTooltipService, DataLabelService, BubbleService, NavigationLineService, SelectionService, AnnotationsService, ZoomService, MapsComponent} from '@syncfusion/ej2-angular-maps';
// import { AgmCoreModule } from '@agm/core';

@NgModule({
  imports: [
    CommonModule,
    // AgmCoreModule.forRoot({
    //   apiKey: 'AIzaSyCtf5GuVDzKh5dcxeRyOzmQ39Cn97R9czI',
    //   language: "fr",
    //   libraries: ['places','geometry']
    // })
  ],
  declarations: [
    MapComponent
  ],
  exports: [
    MapComponent
  ],
  providers:[
    // MapsComponent,
    // LegendService,
    // MarkerService,
    // MapsTooltipService,
    // DataLabelService,
    // BubbleService,
    // NavigationLineService,
    // SelectionService,
    // AnnotationsService,
    // ZoomService
  ]
})
export class MapModule{
  constructor(){}
}
