import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';

import {EvolutionAnalyserComponent} from "./evolution-analyser.component"
@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    EvolutionAnalyserComponent
  ],
  exports: [
    EvolutionAnalyserComponent
  ]
})

export class EvolutionAnalyserModule{
  constructor() {}
}
