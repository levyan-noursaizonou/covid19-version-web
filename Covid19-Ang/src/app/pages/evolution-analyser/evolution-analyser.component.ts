import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-evolution-analyser',
  templateUrl: './evolution-analyser.component.html',
  styleUrls: ['./evolution-analyser.component.scss']
})
export class EvolutionAnalyserComponent implements OnInit {
  myFileOne = 'Progression.json';
  fileUrlOne = '../../../assets/img/img_reg/Progression.json'
  //
  myFileTwo = 'Progression.svg';
  fileUrlTwo = '../../../assets/img/img_reg/Progression.svg'
  constructor() { }

  ngOnInit(): void {
  }

}
