import { AfterViewInit, Component, OnInit,ViewChild } from '@angular/core';
// import { Z_DATA_ERROR } from 'zlib';
import {MatPaginator, MatPaginatorModule} from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import Chart from 'chart.js';
const Graph= [
  {
    "case_communautaire": 2,
    "localite_id": 2,
    "localite_name": "Diamniadio",
    "x": [
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Sat, 13 Mar 2021",
      "Sat, 13 Mar 2021",
      "Sat, 13 Mar 2021"
    ],
    "y": [100, 70, 90, 70, 85, 60, 75, 60, 90, 80, 110, 100]
  },
  {
    "case_communautaire": 2,
    "localite_id": 3,
    "localite_name": "Keurmassar",
    "x": [
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Sat, 13 Mar 2021",
      "Sat, 13 Mar 2021",
      "Sat, 13 Mar 2021"
    ],
    "y": [100, 70, 90, 70, 85, 60, 75, 60, 90, 80, 110, 100]
  },
  {
    "case_communautaire": 2,
    "localite_id": 4,
    "localite_name": "Plateau",
    "x": [
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Sat, 13 Mar 2021",
      "Sat, 13 Mar 2021",
      "Sat, 13 Mar 2021"
    ],
    "y": [100, 70, 90, 70, 85, 60, 75, 60, 90, 80, 110, 100]
  },
  {
    "case_communautaire": 1,
    "localite_id": 5,
    "localite_name": "Gueuletap\u00e3\u00a9e",
    "x": [
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Sat, 13 Mar 2021",
      "Sat, 13 Mar 2021",
      "Sat, 13 Mar 2021"
    ],
    "y": [100, 70, 90, 70, 85, 60, 75, 60, 90, 80, 110, 100]
  },
  {
    "case_communautaire": 1,
    "localite_id": 6,
    "localite_name": "M\u00e3\u00a9dina",
    "x": [
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Sat, 13 Mar 2021",
      "Sat, 13 Mar 2021",
      "Sat, 13 Mar 2021"
    ],
    "y": [100, 70, 90, 70, 85, 60, 75, 60, 90, 80, 110, 100]
  },
  {
    "case_communautaire": 1,
    "localite_id": 7,
    "localite_name": "Sicapkarack",
    "x": [
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Sat, 13 Mar 2021",
      "Sat, 13 Mar 2021",
      "Sat, 13 Mar 2021"
    ],
    "y": [100, 70, 90, 70, 85, 60, 75, 60, 90, 80, 110, 100]
  },
  {
    "case_communautaire": 2,
    "localite_id": 8,
    "localite_name": "Kolda",
    "x": [
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Sat, 13 Mar 2021",
      "Sat, 13 Mar 2021",
      "Sat, 13 Mar 2021"
    ],
    "y": [100, 70, 90, 70, 85, 60, 75, 60, 90, 80, 110, 100]
  },
  {
    "case_communautaire": 2,
    "localite_id": 9,
    "localite_name": "Matam",
    "x": [
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Sat, 13 Mar 2021",
      "Sat, 13 Mar 2021",
      "Sat, 13 Mar 2021"
    ],
    "y": [100, 70, 90, 70, 85, 60, 75, 60, 90, 80, 110, 100]
  },
  {
    "case_communautaire": 2,
    "localite_id": 10,
    "localite_name": "V\u00e3\u00a9lingara",
    "x": [
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Sat, 13 Mar 2021",
      "Sat, 13 Mar 2021",
      "Sat, 13 Mar 2021"
    ],
    "y": [100, 70, 90, 70, 85, 60, 75, 60, 90, 80, 110, 100]
  },
  {
    "case_communautaire": 2,
    "localite_id": 11,
    "localite_name": "Guediawaye",
    "x": [
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Sat, 13 Mar 2021",
      "Sat, 13 Mar 2021",
      "Sat, 13 Mar 2021"
    ],
    "y": [100, 70, 90, 70, 85, 60, 75, 60, 90, 80, 110, 100]  },
  {
    "case_communautaire": 6,
    "localite_id": 12,
    "localite_name": "Pikine",
    "x": [
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Sat, 13 Mar 2021",
      "Sat, 13 Mar 2021",
      "Sat, 13 Mar 2021"
    ],
    "y": [100, 70, 90, 70, 85, 60, 75, 60, 90, 80, 110, 100]  },
  {
    "case_communautaire": 2,
    "localite_id": 13,
    "localite_name": "Thies",
    "x": [
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Tue, 13 Apr 2021",
      "Sat, 13 Mar 2021",
      "Sat, 13 Mar 2021",
      "Sat, 13 Mar 2021"
    ],
    "y": [100, 70, 90, 70, 85, 60, 75, 60, 90, 80, 110, 100]  }
]
@Component({
  selector: 'app-departement',
  templateUrl: './departement.component.html',
  styleUrls: ['./departement.component.scss']
})
export class DepartementComponent implements OnInit,AfterViewInit {
  public canvas : any;
  public ctx;
  public datasets: any;
  public data: any;
  public myChartData;
  public clicked: boolean = true;
  public clicked1: boolean = false;
  public clicked2: boolean = false;
  public g=Graph;
  public myChart;
  public myChartable= [];
  constructor() { }
  // @ViewChild(MatPaginator) paginator: MatPaginator;
  // dataSource = new MatTableDataSource(this.g);
    p: number = 1;
    collection: any[] =this.g;

  ngAfterViewInit(){
  // this.drawChart(this.g[0].localite_name,this.g[0].x,this.g[0].y)
    let i =0;
  // this.drawChart(this.g[1].localite_name,this.g[1].x,this.g[1].y)
  this.g.map((element)=>{
    console.log(element);
    this.myChart=this.drawChart(element.localite_name,element.x,element.y)
    this.myChartable[i++]=this.myChart
    // console.log(this.myChart);
  })
  console.log(this.myChartable);
  // let lengthh=this.g.length;
  // console.log(lengthh);
  // this.canvas = document.getElementById("chartg1");
  // this.ctx = this.canvas.getContext("2d");
  // var gradientStroke = this.ctx.createLinearGradient(0, 230, 0, 50);

  // gradientStroke.addColorStop(1, 'rgba(233,32,16,0.2)');
  // gradientStroke.addColorStop(0.4, 'rgba(233,32,16,0.0)');
  // gradientStroke.addColorStop(0, 'rgba(233,32,16,0)'); //red colors

  //   var data = {
  //   labels:  ['JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'],
  //   datasets: [{
  //     label: "Data Covid-19",
  //     fill: true,
  //     backgroundColor: gradientStroke,
  //     borderColor: '#ec250d',
  //     borderWidth: 2,
  //     borderDash: [],
  //     borderDashOffset: 0.0,
  //     pointBackgroundColor: '#ec250d',
  //     pointBorderColor: 'rgba(255,255,255,0)',
  //     pointHoverBackgroundColor: '#ec250d',
  //     pointBorderWidth: 20,
  //     pointHoverRadius: 4,
  //     pointHoverBorderWidth: 15,
  //     pointRadius: 4,
  //     data:  [80, 100, 70, 80, 120, 80],
  //   }]
  // };

  // var gradientChartOptionsConfigurationWithTooltipRed: any = {
  //   maintainAspectRatio: false,
  //   legend: {
  //     display: true
  //   },

  //   tooltips: {
  //     backgroundColor: '#f5f5f5',
  //     titleFontColor: '#333',
  //     bodyFontColor: '#666',
  //     bodySpacing: 7,
  //     xPadding: 12,
  //     mode: "nearest",
  //     intersect: 0,
  //     position: "nearest"
  //   },
  //   responsive: true,
  //   scales: {
  //     yAxes: [{
  //       barPercentage: 1.6,
  //       gridLines: {
  //         drawBorder: false,
  //         color: 'rgba(29,140,248,0.0)',
  //         zeroLineColor: "transparent",
  //       },
  //       ticks: {
  //         suggestedMin: 60,
  //         suggestedMax: 125,
  //         padding: 20,
  //         fontColor: "#9a9a9a"
  //       }
  //     }],

  //     xAxes: [{
  //       barPercentage: 2.6,
  //       gridLines: {
  //         drawBorder: false,
  //         color: 'rgba(233,32,16,0.1)',
  //         zeroLineColor: "transparent"
  //       },
  //       ticks: {
  //         padding: 20,
  //         fontColor: "#9a9a9a"
  //       }
  //     }]
  //   }
  // };
  // var gradientChartOptionsConfigurationWithTooltipPurple: any = {
  //   maintainAspectRatio: false,
  //   legend: {
  //     display: false
  //   },

  //   tooltips: {
  //     backgroundColor: '#f5f5f5',
  //     titleFontColor: '#333',
  //     bodyFontColor: '#666',
  //     bodySpacing: 4,
  //     xPadding: 12,
  //     mode: "nearest",
  //     intersect: 0,
  //     position: "nearest"
  //   },
  //   responsive: true,
  //   scales: {
  //     yAxes: [{
  //       barPercentage: 1.6,
  //       gridLines: {
  //         drawBorder: false,
  //         color: 'rgba(29,140,248,0.0)',
  //         zeroLineColor: "transparent",
  //       },
  //       ticks: {
  //         suggestedMin: 60,
  //         suggestedMax: 125,
  //         padding: 20,
  //         fontColor: "#9a9a9a"
  //       }
  //     }],

  //     xAxes: [{
  //       barPercentage: 1.6,
  //       gridLines: {
  //         drawBorder: false,
  //         color: 'rgba(225,78,202,0.1)',
  //         zeroLineColor: "transparent",
  //       },
  //       ticks: {
  //         padding: 20,
  //         fontColor: "#9a9a9a"
  //       }
  //     }]
  //   }
  // };
  // var myChart = new Chart(this.ctx, {
  //   type: 'line',
  //   data: data,
  //   options: gradientChartOptionsConfigurationWithTooltipRed
  // });
  }

  ngOnInit(): void {
    // this.dataSource.paginator=this.paginator;
    // console.log(this.dataSource)
  // var gradientChartOptionsConfigurationWithTooltipBlue: any = {
  //   maintainAspectRatio: false,
  //   legend: {
  //     display: false
  //   },

  //   tooltips: {
  //     backgroundColor: '#f5f5f5',
  //     titleFontColor: '#333',
  //     bodyFontColor: '#666',
  //     bodySpacing: 4,
  //     xPadding: 12,
  //     mode: "nearest",
  //     intersect: 0,
  //     position: "nearest"
  //   },
  //   responsive: true,
  //   scales: {
  //     yAxes: [{
  //       barPercentage: 1.6,
  //       gridLines: {
  //         drawBorder: false,
  //         color: 'rgba(29,140,248,0.0)',
  //         zeroLineColor: "transparent",
  //       },
  //       ticks: {
  //         suggestedMin: 60,
  //         suggestedMax: 125,
  //         padding: 20,
  //         fontColor: "#23[80]f7"
  //       }
  //     }],

  //     xAxes: [{
  //       barPercentage: 1.6,
  //       gridLines: {
  //         drawBorder: false,
  //         color: 'rgba(29,140,248,0.1)',
  //         zeroLineColor: "transparent",
  //       },
  //       ticks: {
  //         padding: 20,
  //         fontColor: "#23[80]f7"
  //       }
  //     }]
  //   }
  // };

  // var gradientChartOptionsConfigurationWithTooltipPurple: any = {
  //   maintainAspectRatio: false,
  //   legend: {
  //     display: false
  //   },

  //   tooltips: {
  //     backgroundColor: '#f5f5f5',
  //     titleFontColor: '#333',
  //     bodyFontColor: '#666',
  //     bodySpacing: 4,
  //     xPadding: 12,
  //     mode: "nearest",
  //     intersect: 0,
  //     position: "nearest"
  //   },
  //   responsive: true,
  //   scales: {
  //     yAxes: [{
  //       barPercentage: 1.6,
  //       gridLines: {
  //         drawBorder: false,
  //         color: 'rgba(29,140,248,0.0)',
  //         zeroLineColor: "transparent",
  //       },
  //       ticks: {
  //         suggestedMin: 60,
  //         suggestedMax: 125,
  //         padding: 20,
  //         fontColor: "#9a9a9a"
  //       }
  //     }],

  //     xAxes: [{
  //       barPercentage: 1.6,
  //       gridLines: {
  //         drawBorder: false,
  //         color: 'rgba(225,78,202,0.1)',
  //         zeroLineColor: "transparent",
  //       },
  //       ticks: {
  //         padding: 20,
  //         fontColor: "#9a9a9a"
  //       }
  //     }]
  //   }
  // };

  // var gradientChartOptionsConfigurationWithTooltipRed: any = {
  //   maintainAspectRatio: false,
  //   legend: {
  //     display: false
  //   },

  //   tooltips: {
  //     backgroundColor: '#f5f5f5',
  //     titleFontColor: '#333',
  //     bodyFontColor: '#666',
  //     bodySpacing: 4,
  //     xPadding: 12,
  //     mode: "nearest",
  //     intersect: 0,
  //     position: "nearest"
  //   },
  //   responsive: true,
  //   scales: {
  //     yAxes: [{
  //       barPercentage: 1.6,
  //       gridLines: {
  //         drawBorder: false,
  //         color: 'rgba(29,140,248,0.0)',
  //         zeroLineColor: "transparent",
  //       },
  //       ticks: {
  //         suggestedMin: 60,
  //         suggestedMax: 125,
  //         padding: 20,
  //         fontColor: "#9a9a9a"
  //       }
  //     }],

  //     xAxes: [{
  //       barPercentage: 1.6,
  //       gridLines: {
  //         drawBorder: false,
  //         color: 'rgba(233,32,16,0.1)',
  //         zeroLineColor: "transparent",
  //       },
  //       ticks: {
  //         padding: 20,
  //         fontColor: "#9a9a9a"
  //       }
  //     }]
  //   }
  // };

  // var gradientChartOptionsConfigurationWithTooltipOrange: any = {
  //   maintainAspectRatio: false,
  //   legend: {
  //     display: false
  //   },

  //   tooltips: {
  //     backgroundColor: '#f5f5f5',
  //     titleFontColor: '#333',
  //     bodyFontColor: '#666',
  //     bodySpacing: 4,
  //     xPadding: 12,
  //     mode: "nearest",
  //     intersect: 0,
  //     position: "nearest"
  //   },
  //   responsive: true,
  //   scales: {
  //     yAxes: [{
  //       barPercentage: 1.6,
  //       gridLines: {
  //         drawBorder: false,
  //         color: 'rgba(29,140,248,0.0)',
  //         zeroLineColor: "transparent",
  //       },
  //       ticks: {
  //         suggestedMin: 50,
  //         suggestedMax: 110,
  //         padding: 20,
  //         fontColor: "#ff8a76"
  //       }
  //     }],

  //     xAxes: [{
  //       barPercentage: 1.6,
  //       gridLines: {
  //         drawBorder: false,
  //         color: 'rgba(220,53,69,0.1)',
  //         zeroLineColor: "transparent",
  //       },
  //       ticks: {
  //         padding: 20,
  //         fontColor: "#ff8a76"
  //       }
  //     }]
  //   }
  // };

  // var gradientChartOptionsConfigurationWithTooltipGreen: any = {
  //   maintainAspectRatio: false,
  //   legend: {
  //     display: false
  //   },

  //   tooltips: {
  //     backgroundColor: '#f5f5f5',
  //     titleFontColor: '#333',
  //     bodyFontColor: '#666',
  //     bodySpacing: 4,
  //     xPadding: 12,
  //     mode: "nearest",
  //     intersect: 0,
  //     position: "nearest"
  //   },
  //   responsive: true,
  //   scales: {
  //     yAxes: [{
  //       barPercentage: 1.6,
  //       gridLines: {
  //         drawBorder: false,
  //         color: 'rgba(29,140,248,0.0)',
  //         zeroLineColor: "transparent",
  //       },
  //       ticks: {
  //         suggestedMin: 50,
  //         suggestedMax: 125,
  //         padding: 20,
  //         fontColor: "#9e9e9e"
  //       }
  //     }],

  //     xAxes: [{
  //       barPercentage: 1.6,
  //       gridLines: {
  //         drawBorder: false,
  //         color: 'rgba(0,242,195,0.1)',
  //         zeroLineColor: "transparent",
  //       },
  //       ticks: {
  //         padding: 20,
  //         fontColor: "#9e9e9e"
  //       }
  //     }]
  //   }
  // };

  // var gradientBarChartConfiguration: any = {
  //   maintainAspectRatio: false,
  //   legend: {
  //     display: false
  //   },

  //   tooltips: {
  //     backgroundColor: '#f5f5f5',
  //     titleFontColor: '#333',
  //     bodyFontColor: '#666',
  //     bodySpacing: 4,
  //     xPadding: 12,
  //     mode: "nearest",
  //     intersect: 0,
  //     position: "nearest"
  //   },
  //   responsive: true,
  //   scales: {
  //     yAxes: [{

  //       gridLines: {
  //         drawBorder: false,
  //         color: 'rgba(29,140,248,0.1)',
  //         zeroLineColor: "transparent",
  //       },
  //       ticks: {
  //         suggestedMin: 60,
  //         suggestedMax: 120,
  //         padding: 20,
  //         fontColor: "#9e9e9e"
  //       }
  //     }],

  //     xAxes: [{

  //       gridLines: {
  //         drawBorder: false,
  //         color: 'rgba(29,140,248,0.1)',
  //         zeroLineColor: "transparent",
  //       },
  //       ticks: {
  //         padding: 20,
  //         fontColor: "#9e9e9e"
  //       }
  //     }]
  //   }
  // };
  // this.g.map((element)=>{
  //   console.log(element);
  //   this.drawChart(element.localite_name,element.x,element.y)
  // })
  // this.drawChart(this.g[0].localite_name,this.g[0].x,this.g[0].y)
}

public drawChart(_elements_: any,_label: any,_dataLabel: any){
  // console.log(_elements_);
  this.canvas = document.getElementById(_elements_);
  // console.log(this.canvas);
  // console.log(document.getElementById("Diamniadio"));
  // console.log(document.getElementsByTagName("canvas"));
  // console.log(document.getElementsByClassName(_elements_));
  this.ctx = this.canvas.getContext("2d");
  var gradientStroke = this.ctx.createLinearGradient(0, 230, 0, 50);

  gradientStroke.addColorStop(1, 'rgba(233,32,16,0.2)');
  gradientStroke.addColorStop(0.4, 'rgba(233,32,16,0.0)');
  gradientStroke.addColorStop(0, 'rgba(233,32,16,0)'); //red colors

  var gradientChartOptionsConfigurationWithTooltipRed: any = {
    maintainAspectRatio: false,
    legend: {
      display: false
    },

    tooltips: {
      backgroundColor: '#f5f5f5',
      titleFontColor: '#333',
      bodyFontColor: '#666',
      bodySpacing: 7,
      xPadding: 12,
      mode: "nearest",
      intersect: 0,
      position: "nearest"
    },
    responsive: true,
    scales: {
      yAxes: [{
        barPercentage: 1.6,
        gridLines: {
          drawBorder: false,
          color: 'rgba(29,140,248,0.0)',
          zeroLineColor: "transparent",
        },
        ticks: {
          suggestedMin: 60,
          suggestedMax: 125,
          padding: 20,
          fontColor: "#9a9a9a"
        }
      }],

      xAxes: [{
        barPercentage: 2.6,
        gridLines: {
          drawBorder: false,
          color: 'rgba(233,32,16,0.1)',
          zeroLineColor: "transparent",
          type: 'linear',
          position: 'bottom'
        },
        ticks: {
          padding: 20,
          fontColor: "#9a9a9a"
        }
      }]
    }
  };

  // var data = {
  //   labels: _label,
  //   datasets: [{
  //     label: "Data Covid-19",
  //     fill: true,
  //     backgroundColor: gradientStroke,
  //     borderColor: '#ec250d',
  //     borderWidth: 2,
  //     borderDash: [],
  //     borderDashOffset: 0.0,
  //     pointBackgroundColor: '#ec250d',
  //     pointBorderColor: 'rgba(255,255,255,0)',
  //     pointHoverBackgroundColor: '#ec250d',
  //     pointBorderWidth: 20,
  //     pointHoverRadius: 4,
  //     pointHoverBorderWidth: 15,
  //     pointRadius: 4,
  //     data:  _dataLabel,
  //   }]
  // };
  var config = {
    type: 'line',
    data: {
      labels: _label,
      datasets: [{
        label: "Data Covid-19",
        fill: true,
        backgroundColor: gradientStroke,
        borderColor: '#ec250d',
        borderWidth: 2,
        borderDash: [],
        borderDashOffset: 0.0,
        pointBackgroundColor: '#ec250d',
        pointBorderColor: 'rgba(255,255,255,0)',
        pointHoverBackgroundColor: '#ec250d',
        pointBorderWidth: 20,
        pointHoverRadius: 4,
        pointHoverBorderWidth: 15,
        pointRadius: 4,
        data:  _dataLabel,
      }]
    },
    options: gradientChartOptionsConfigurationWithTooltipRed
  };

  this.myChart = new Chart(this.ctx,config
  //   {
  //   type: 'line',
  //   data: data,
  //   options: gradientChartOptionsConfigurationWithTooltipRed
  // }
  );
  return this.myChart;
}

public updateOptions() {
  // this.myChartData.data.datasets[0].data = this.data;
  this.myChartable.forEach(element => {
    if (this.clicked===true && this.clicked1===false && this.clicked2===false) {
      element.config.type='line';
      console.log(element.config.type);
      element.update();
      }
      else{
        if (this.clicked===false && this.clicked1===true && this.clicked2===false) {
      element.config.type='bar';
      console.log(element.config.type);
      element.update();

        }else{
          if (this.clicked===false && this.clicked1===false && this.clicked2===true) {
            element.config.type='scatter';
            console.log(element.config.type);
            element.update();
          }
        }
      }
  });
  // this.myChart.update();
}

}
