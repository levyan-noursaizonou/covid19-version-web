import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';

import {DepartementComponent} from '../departement/departement.component'
import {MatPaginatorModule} from '@angular/material/paginator';
import {MaterialModule} from '../material/material.module';
import {NgxPaginationModule} from 'ngx-pagination';
@NgModule({
  imports: [CommonModule,MaterialModule,NgxPaginationModule],
  declarations: [DepartementComponent],
  exports: [DepartementComponent]
})
export class DepartementModule {
constructor() {}
}
