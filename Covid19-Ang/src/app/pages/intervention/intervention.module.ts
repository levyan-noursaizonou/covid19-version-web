import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';

import {InterventionComponent} from '../intervention/intervention.component'
@NgModule({
  imports: [CommonModule],
  declarations: [InterventionComponent],
  exports: [InterventionComponent]
})
export class InterventionModule{
  constructor() {}
}
