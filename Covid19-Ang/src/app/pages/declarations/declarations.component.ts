import { Component, OnInit } from '@angular/core';
import { faFolder, faFolderOpen, faSquare, faCheckSquare, faCheck, faMinus } from '@fortawesome/free-solid-svg-icons';
/**
 * Declarations data with nested structure.
 * Each node has a name and an optional list of children.
 *  name: 'Publication du 13-03-2021',
    children: {
      url: 'https://sante.sec.gouv.sn/Actualites/coronavirus-communiqu%C3%A9-de-presse-n%C2%B0408-du-mardi-13-avril-2021-du-minist%C3%A8re-de-la-sant%C3%A9-et',
      date_publication: '2021-04-13',
      nombre_tests: 999,
      nombre_cas_positifs: 80,
      nombre_cas_contacts: 13,
      nombre_cas_importes: 0,
      nombre_cas_graves: 14,
      nombre_patients: 26,
      nombre_deces: 14
 */
      const Arbre = [
        {
          label: 'Publication du 13-03-2021',
          value: 1,
          children: [
            {
              label: 'Url',
              value: '0',
              children: [
                {
                  label: 'https://sante.sec.gouv.sn/Actualites/coronavirus-communiqu%C3%A9-de-presse'
                }
              ]
            },
            {
              label: 'Date_Publication',
              value: '1-1',
              children: [
                {
                  label: '2021-04-13'
                }
              ]
            },
            {
              label: 'Nombre_Tests',
              value: '1-2',
              children: [
                {
                  label: '999'
                }
              ]
            },
            {
              label: 'Nombre_Cas_Positifs',
              value: '1-3',
              children: [
                {
                  label: '80'
                }
              ]
            },
            {
              label: 'Nombre_Cas_Contacts',
              value: '1-4',
              children: [
                {
                  label: '13'
                }
              ]
            },
            {
              label: 'Nombre_Cas_Importes',
              value: '1-5',
              children: [
                {
                  label: '0'
                }
              ]
            },
            {
              label: 'Nombre_Cas_Graves',
              value: '1-6',
              children: [
                {
                  label: '14'
                }
              ]
            },
            {
              label: 'Nombre_Patients',
              value: '1-7',
              children: [
                {
                  label: '26'
                }
              ]
            },
            {
              label: 'Nombre_Deces',
              value: '1-8',
              children: [
                {
                  label: '14'
                }
              ]
            },
          ]
        }
      ]
      const roots = [
        {
            label: 'Langages de programmation',
            value: 1,
            children: [
                {
                    label: 'C++',
                    value: 11
                },
                {
                    label: 'Angular',
                    value: 12
                },
                {
                    label: 'C#',
                    value: 13,
                    children: [
                        {
                            label: 'LinQ',
                            value: 131
                        },
                        {
                            label: 'UWP',
                            value: 132
                        },
                        {
                            label: 'Sharepoint',
                            value: 133
                        },
                        {
                            label: 'WPF',
                            value: 134
                        }
                    ]
                },
                {
                    label: 'Java',
                    value: 14,
                    children: [
                        {
                            label: 'J2E',
                            value: 141
                        },
                        {
                            label: 'Spring Framework',
                            value: 142
                        },
                        {
                            label: 'Vanilla Java',
                            value: 143
                        },
                        {
                            label: 'Android',
                            value: 144
                        }
                    ]
                },
                {
                    label: 'Empty folder test',
                    value: 15,
                    children: []
                }
            ]
        }, {
            value: 1111,
            label: 'Customers',
            children: [
                {
                    label: 'Norton',
                    value: 156
                },
                {
                    label: 'Symantec',
                    value: 116
                },
                {
                    label: 'Some company',
                    value: 126
                },
                {
                    label: 'Zokelion',
                    value: 196
                }
            ]
        }
    ]
@Component({
  selector: 'app-declarations',
  templateUrl: './declarations.component.html',
  styleUrls: ['./declarations.component.scss']
})
export class DeclarationsComponent implements OnInit {

  // Register them into your component
  public faFolder = faFolder;
  public faFolderOpen = faFolderOpen;
  public faSquare = faSquare;
  public faCheckSquare = faCheckSquare;
  public faMinus = faMinus;
  public faCheck = faCheck;
  public trees=Arbre;
  constructor() { }

  ngOnInit(): void {
  }

}
