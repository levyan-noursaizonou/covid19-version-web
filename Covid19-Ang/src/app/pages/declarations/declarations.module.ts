import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';

import {DeclarationsComponent} from '../declarations/declarations.component'
// import {MaterialModule} from '../material/material.module'
import {MatProgressBarModule} from '@angular/material/progress-bar';
import { NgxBootstrapTreeviewModule } from 'ngx-bootstrap-treeview';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  imports: [CommonModule,NgxBootstrapTreeviewModule],
  declarations: [DeclarationsComponent],
  exports: [DeclarationsComponent]
})

export class DeclarationsModules {
 constructor() {}
}
