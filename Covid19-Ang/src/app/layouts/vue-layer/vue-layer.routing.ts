import { Routes } from "@angular/router";

// import {VueLayerRoutes} from "./vue-layer.routing";
import {DeclarationsComponent} from "../../pages/declarations/declarations.component";
import {DepartementComponent} from "../../pages/departement/departement.component";
import {EvolutionAnalyserComponent} from "../../pages/evolution-analyser/evolution-analyser.component";
import {InterventionComponent} from "../../pages/intervention/intervention.component";
import {MapComponent} from "../../pages/map/maps.component";
import {RegionsComponent} from "../../pages/regions/regions.component";
import {UsersComponent} from "../../pages/users/users.component";
import {Covid19IndexComponent} from '../../pages/covid19-index/covid19-index.component';
/// <amd-module name="@angular/compiler-cli/ngcc/src/"

export const VueLayerRoutes: Routes=[
  {
    path: "declarations",component:DeclarationsComponent
  },
  {
    path: "departments",component:DepartementComponent
  },
  {
    path: "evolution-analyser",component:EvolutionAnalyserComponent
  },
  {
    path: "interventions",component: InterventionComponent
  },
  {
    path: "maps",component:MapComponent
  },
  {
    path: "regions",component:RegionsComponent
  },
  {
    path: "users",component:UsersComponent
  },
  {
    path: "Covid19",component:Covid19IndexComponent
  }
 // { path: "rtl", component: RtlComponent }
]
