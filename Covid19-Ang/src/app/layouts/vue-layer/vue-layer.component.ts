import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-vue-layer',
  templateUrl: './vue-layer.component.html',
  styleUrls: ['./vue-layer.component.scss']
})
export class VueLayerComponent implements OnInit {
  public sidebarColor: string = "red";
  constructor() { }

  ngOnInit(): void {
  }

  changeSidebarColor(color: string){
    var sidebar = document.getElementsByClassName('sidebar')[0];
    var mainPanel = document.getElementsByClassName('main-panel')[0];
    // console.log(mainPanel);
    // console.log(sidebar);

    this.sidebarColor = color;

    if(sidebar != undefined){
        sidebar.setAttribute('data',color);
    }
    if(mainPanel != undefined){
        mainPanel.setAttribute('data',color);
    }
  }

  changeDashboardColor(bodyclass: string){
    var body = document.getElementsByTagName('body')[0];
    if(body && bodyclass === 'white-content'){
      body.classList.add(bodyclass);
    }
    else {
      if(body.classList.contains('white-content')){
        body.classList.remove('white-content')
      }
    }
  }
}
