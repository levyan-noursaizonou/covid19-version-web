import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { RouterModule } from "@angular/router";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import {VueLayerRoutes} from "./vue-layer.routing";
import {EvolutionAnalyserModule} from "../../pages/evolution-analyser/evolution-analyser.module";
import {MapModule} from "../../pages/map/maps.module";
// import {RegionsComponent} from "../../pages/regions/regions.component";
import {UsersComponent} from "../../pages/users/users.component";
/// <amd-module name="@angular/compiler-cli/ngcc/src/"
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { DeclarationsModules } from "src/app/pages/declarations/declarations.module";
import { DepartementModule } from "src/app/pages/departement/departement.module";
import { InterventionModule } from "src/app/pages/intervention/intervention.module";
import { RegionsModule } from "src/app/pages/regions/regions.module";
import { UsersModule } from "src/app/pages/users/users.module";
// import { MaterialModule } from "src/app/pages/material/material.module";
import {NgxPaginationModule} from 'ngx-pagination';
// import { AgmCoreModule } from '@agm/core';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(VueLayerRoutes),
    FormsModule,
    HttpClientModule,
    NgbModule,
    MapModule,
    EvolutionAnalyserModule,
    DeclarationsModules,
    DepartementModule,
    InterventionModule,
    RegionsModule,
    UsersModule,
    NgxPaginationModule
    // MaterialModule
  ],
  declarations: [
    // RegionsComponent,
    // UsersComponent
  ]
})

export class VueLayerModule {
  // constructor() {}
}
