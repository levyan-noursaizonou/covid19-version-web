import flask
import json
from flask import request, jsonify
from flask_mysqldb import MySQL
import datetime
import shutil

app = flask.Flask(__name__)
app.config["DEBUG"] = True
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = 'passer123'
app.config['MYSQL_DB'] = 'covid'

mysql = MySQL(app)

@app.route('/', methods=['GET'])
def home():
    return TransmissionData();

@app.route('/covidHome/data', methods=['GET'])
def data():
    #mysql=MySQL()
    #mysql=MySQL(app, prefix="mysql_connect", host="localhost", user="root",password="passer123",db="covid", autocommit=True)
    #cursor = mysql.connection.cursor()
    cur = mysql.connection.cursor()
    cur.execute('''SELECT user, host FROM mysql.user''')
    rv = cur.fetchall()
    return str(rv)


@app.route('/covidHome/data/declaration', methods=['GET'])
def DeclarationData():
    dictdcl={'declaration':[]}
    cur = mysql.connection.cursor()
    cur.execute('''SELECT * FROM covid.declaration''')
    dcl = cur.fetchall()
    # find dictionnaire
    for d in dcl:
        dictdcl["declaration"].append({
            'id': d[0],
            'nom': d[1],
            'url': d[2],
            'date_publication': d[3],
            'nombre_tests': d[4],
            'nombre_tests': d[5],
            'nombre_cas_contacts': d[6],
            'nombre_cas_importes': d[7],
            'nombre_cas_graves': d[8],
            'nombre_patients': d[9],
            'nombre_patients': d[10]})
    return dictdcl

@app.route('/covidHome/data/Localite', methods=['GET'])
def LocaliteData():
    dictLc={'Localite':[]}
    cur = mysql.connection.cursor()
    cur.execute('''SELECT * FROM covid.localite''')
    Lc = cur.fetchall()
    for l in Lc:
        dictLc['Localite'].append({
            'id': l[0],
            'nom': l[1],
            'ville_id': l[2]
        })
    return dictLc

@app.route('/covidHome/data/Ville', methods=['GET'])
def CityData():
    dictVl={'City':[]}
    cur = mysql.connection.cursor()
    cur.execute('''SELECT * FROM covid.ville''')
    vl = cur.fetchall()
    for v in vl:

        nompi=v[1];
        # print(nompi); requete 1
        cur.execute(f"""SELECT SUM(s.nombre_cas) as nombre_case,s.city 
        FROM (SELECT transmission_communautaire.nombre_cas,localite.nom as localite , ville.nom as city 
        FROM transmission_communautaire,localite,ville 
        WHERE transmission_communautaire.localite_id=localite.id 
        AND localite.ville_id=ville.id) s 
        WHERE s.city='{nompi}' """)
        v2=cur.fetchall();

        # print(str(v2));requette2
        cur.execute(f"""SELECT s.localite ,s.nombre_cas
        FROM (SELECT transmission_communautaire.nombre_cas,localite.nom as localite , ville.nom as city 
        FROM transmission_communautaire,localite,ville 
        WHERE transmission_communautaire.localite_id=localite.id 
        AND localite.ville_id=ville.id) s 
        WHERE s.city='{nompi}' """)
        v3=cur.fetchall();
        # print(v3[0]);
        localite=[]
        for i in range(len(v3)):
            localite.append({
                'name': v3[i][0],
                'case': v3[i][1]
            })
        # print(localite)
        dictVl['City'].append({
            'id': v[0],
            'nom': v[1],
            'nombre_cas': str(v2[0][0]),
            'departement_list': localite
        })

    return dictVl

@app.route('/covidHome/data/Transmission-Communautaire', methods=['GET']) 
def queryWithLocalite(localite_id: int):
    case = None
    cursor = mysql.connection.cursor()
    sql = f"""SELECT t.*,d.id AS localite_id, d.nom AS localite_nom
        from (SELECT t.id, t.nombre_cas as case_communautaire, t.localite_id,d.id AS declaration_id,  
                        d.nom AS declaration_nom,  
                        d.url AS declaration_url,  
                        d.date_publication AS declaration_date_publication,     
                        d.nombre_tests as declaration_nombre_tests,  
                        d.nombre_cas_positifs AS declaration_nombre_cas_positifs,   
                        d.nombre_cas_contacts AS declaration_nombre_cas_contacts,   
                        d.nombre_cas_importes AS declaration_nombre_cas_importes,   
                        d.nombre_cas_graves AS declaration_nombre_cas_graves,   
                        d.nombre_patients AS declaration_nombre_patients,   
                        d.nombre_deces AS declaration_nombre_deces     
        FROM covid.transmission_communautaire t
        JOIN covid.declaration d ON t.localite_id={localite_id} AND t.declaration_id=d.id
        ORDER BY d.date_publication DESC LIMIT 1) t
        JOIN covid.localite d ON t.localite_id=d.id
                """
    cursor.execute(sql)
    response = cursor.fetchone()
    keys = map(lambda x: x[0].lower(), cursor.description)

    # if response is not None:
    #     case = TransmissionCommunautaire()
    #     case.dump(response, keys)
    return response

def TransmissionData():
    dictTCM={'Graph':[]}
    cur = mysql.connection.cursor()
    cur.execute('''SELECT * FROM covid.transmission_communautaire''')
    tcm = cur.fetchall()
    data_time=[]
    for t in tcm:
        data=queryWithLocalite(t[3]);
        i=0;
        for d in data:
            if(i==6):
                data_time.append(d)
            i=i+1;
        dictTCM['Graph'].append({
            'localite_id': t[3],
            'localite_name': data[15],
            'case_communautaire': data[1],
            'x-val': data_time,
            'y-val': data[8]
        })
        
    return  dictTCM

@app.route('/covidHome/data/evolution-analyser',methods=['GET'])
def loadSvgEvolution():
    # Progression.json
    origin = r'C:\Users\HP\Documents\DIC2\SGBD\covid-19-acquisition_gui\Progression.json';
    target = r'C:\Users\HP\Documents\DIC2\SGBD\projet\covid19-VWeb\Covid19-Ang\src\assets\img\img_reg\Progression.json';
    shutil.copyfile(origin,target);
    # Progression.svg
    origin = r'C:\Users\HP\Documents\DIC2\SGBD\covid-19-acquisition_gui\Progression.svg';
    target = r'C:\Users\HP\Documents\DIC2\SGBD\projet\covid19-VWeb\Covid19-Ang\src\assets\img\img_reg\Progression.svg';
    shutil.copyfile(origin,target);
    return "Copyfile ending...";

app.run()

"""
ng new mon-premier-projet
cd mon-premier-projet
ng serve --open
ng new Covid19-Ang --style=scss --skip-tests=true
npm install bootstrap@3.3.7 --save
ng generate component mon-premier 
la liaison de données, ou "databinding" : la communication entre your code TypeScript et le template HTML
"""